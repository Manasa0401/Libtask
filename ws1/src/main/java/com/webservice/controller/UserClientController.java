package com.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lib.dto.User;
import com.webservice.client.UserOperationsClient;

@RestController
@RequestMapping("/client")
public class UserClientController {
	@Autowired
	UserOperationsClient userOperationsClient;
	

	@PostMapping("/addUser")
	public User createUser(@RequestBody User user) {
		return userOperationsClient.createuser(user);


}
}
