package com.webservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webservice.entity.User;
import com.webservice.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	
	
@Autowired
UserRepository userRepository;
 

public User createUser(User user) {
	return userRepository.save(user);
}
}


